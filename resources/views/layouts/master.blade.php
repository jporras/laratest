<!DOCTYPE html>
<html ng-app="app">
<head>
	<title>Coalition Technologies Laravel Skill Test - Product Web Page</title>
	<link rel="stylesheet" href="{{ asset('css/base.css') }}">
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body ng-controller="ProductController">
	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Coalition Technologies Laravel Skill Test</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="">My Portfolio</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">

      <div class="starter-template">
      <h1>Manage Product</h1>
      </div>

      @yield('content')

    </div><!-- /.container -->
	<script src="{{ asset('js/base.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>