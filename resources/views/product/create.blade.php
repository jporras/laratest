@extends('layouts.master')

@section('content')

    <div class="row">
    	<div class="col-md-4 col-md-offset-4">
    		<h4>Add Product</h4>
    		<form id="add-product-form">	

    		<div ng-show="hasMessage">
    			<p class="alert"></p>
    		</div>

			  <div class="form-group">
			    <label for="name">Name</label>
			    <input type="text" class="form-control" name="name" id="name" placeholder="Product Name">
			  </div>
			  <div class="form-group">
			    <label for="name">Quantity</label>
			    <input type="text" class="form-control" name="quantity" id="quantity" placeholder="Quantity In Stock">
			  </div>
			  <div class="form-group">
			    <label for="name">Price</label>
			    <input type="text" class="form-control" name="price" id="price" placeholder="Price Per Item">
			  </div>
			  <div class="text-right">
			  	<button type="button" class="btn btn-danger" ng-click="saveProduct()">Submit</button>
			  </div>
			</form>
    	</div>
    </div>

    <div class="row" id="products-table-block">
    	<div class="col-md-8 col-md-offset-2">
    		<h4>Product List</h4>
    		<table class="table">
			  <thead>
			  	<th>Name</th>
			  	<th>Quantity</th>
			  	<th>Price</th>
			  	<th>Date Submitted</th>
			  	<th>Total Value No.</th>
			  </thead>
			  <tbody>
			  	<tr ng-repeat="product in products">
			  		<td>@{{ product.name }}</td>
			  		<td>@{{ product.quantity }}</td>
			  		<td>@{{ product.price | currency}}</td>
			  		<td>@{{ product.created_at }}</td>
			  		<td>@{{ product.quantity * product.price }}</td>
			  	</tr>
			  </tbody>
			</table>

			<h4 class="text-right">Sum Total Value No.: @{{sumTotal}}</h4>
    	</div>
    </div>

@endsection