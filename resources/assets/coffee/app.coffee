app = angular.module 'app', []

app.controller 'ProductController', ['$scope', '$http', ($scope, $http)->

	$scope.hasMessage = false

	$scope.products = []

	$scope.sumTotal = 0

	$scope.getProducts = ()->
		$http.get('/api/product')
		.then(
			success = (response)->
				# $scope.products = response.data
				angular.forEach response.data, (product)->
					$scope.sumTotal = $scope.sumTotal + (product.quantity * product.price)
					$scope.products.push(product) 

			error = (response)->
				alert 'Error occured while getting products data.'
		)		

	$scope.saveProduct = ()->

		form = angular.element('#add-product-form').serializeArray()
		formData = new Object()
		angular.forEach form, (obj, key)->
			formData[obj.name] = obj.value

		$http.post('/api/product', formData)
		.then(
			success = (response)->
				$scope.hasMessage = true
				product = response.data
				$scope.products.push product
				$scope.sumTotal = $scope.sumTotal + (product.quantity * product.price)
				$scope.displayMessage 'Product saved.', 'success' 
				angular.element('#add-product-form').get()[0].reset()

			error = (response)->
				$scope.hasMessage = true
				$scope.displayMessage response.data, 'danger' 
		)
	$scope.displayMessage = (message, type)->
		angular.element('.alert').empty().removeClass('alert-danger alert-success').addClass('alert-' + type)
		if angular.isObject(message)
			alert = '<ul>'
			angular.forEach message, (msg, key)->
				alert += '<li>'+msg[0]+'</li>'
			alert += '</ul>'
		else
			alert = message	
		angular.element('.alert').append(alert)

	$scope.getProducts()
]