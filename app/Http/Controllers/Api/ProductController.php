<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateProductRequest;
use Storage;
use App\Product;

class ProductController extends Controller
{
	private $fileName;

	private $product;

	public function __construct(Product $product)
	{
		$this->fileName = 'products.txt';

		$this->product = $product;
	}

	public function index()
	{
		$fileName = $this->fileName;

		$products = [];

		if(Storage::has($fileName))
		{
			$products = json_decode(Storage::get($fileName), true);

			$products = collect($products)->sortByDesc('created_at');
			
			$products = $products->all();
		}

		return $products;
	}

	public function store(CreateProductRequest $request)
	{
		$fileName = $this->fileName;

		$productData = [
			'name' => $request->input('name'),
			'quantity' => $request->input('quantity'),
			'price' => $request->input('price'),
			'created_at' => date('Y-m-d H:i:s'),
		];

		if(Storage::has($fileName) === false)
		{
			$products = [$productData];

			Storage::put($fileName, json_encode($products));
		}
		else
		{
			$products = json_decode(Storage::get($fileName), true);
			$products[] = $productData;
			Storage::put($fileName, json_encode($products));
		}

		return $productData;
	}

}
