var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

	// angular js

	mix.copy('bower_components/angular/angular.min.js', 'resources/assets/js/angular.min.js');

	// bootstrap

	mix.copy('bower_components/bootstrap/dist/css/bootstrap.min.css', 'resources/assets/css/bootstrap.min.css');
	mix.copy('bower_components/bootstrap/dist/js/bootstrap.min.js', 'resources/assets/js/bootstrap.min.js');

	// jquery

	mix.copy('bower_components/jquery/dist/jquery.min.js', 'resources/assets/js/jquery.min.js');

	// compile sass

	mix.sass('app.scss');

	// compile coffee

	mix.coffee('app.coffee')

	// combine styles

    mix.styles([
    	'bootstrap.min.css'
	], 'public/css/base.css');

    // combine scripts

	mix.scripts([
		'jquery.min.js',
		'bootstrap.min.js',
		'angular.min.js'
	], 'public/js/base.js');

});
