(function() {
  var app;

  app = angular.module('app', []);

  app.controller('ProductController', [
    '$scope', '$http', function($scope, $http) {
      $scope.hasMessage = false;
      $scope.products = [];
      $scope.sumTotal = 0;
      $scope.getProducts = function() {
        var error, success;
        return $http.get('/api/product').then(success = function(response) {
          return angular.forEach(response.data, function(product) {
            $scope.sumTotal = $scope.sumTotal + (product.quantity * product.price);
            return $scope.products.push(product);
          });
        }, error = function(response) {
          return alert('Error occured while getting products data.');
        });
      };
      $scope.saveProduct = function() {
        var error, form, formData, success;
        form = angular.element('#add-product-form').serializeArray();
        formData = new Object();
        angular.forEach(form, function(obj, key) {
          return formData[obj.name] = obj.value;
        });
        return $http.post('/api/product', formData).then(success = function(response) {
          var product;
          $scope.hasMessage = true;
          product = response.data;
          $scope.products.push(product);
          $scope.sumTotal = $scope.sumTotal + (product.quantity * product.price);
          $scope.displayMessage('Product saved.', 'success');
          return angular.element('#add-product-form').get()[0].reset();
        }, error = function(response) {
          $scope.hasMessage = true;
          return $scope.displayMessage(response.data, 'danger');
        });
      };
      $scope.displayMessage = function(message, type) {
        var alert;
        angular.element('.alert').empty().removeClass('alert-danger alert-success').addClass('alert-' + type);
        if (angular.isObject(message)) {
          alert = '<ul>';
          angular.forEach(message, function(msg, key) {
            return alert += '<li>' + msg[0] + '</li>';
          });
          alert += '</ul>';
        } else {
          alert = message;
        }
        return angular.element('.alert').append(alert);
      };
      return $scope.getProducts();
    }
  ]);

}).call(this);

//# sourceMappingURL=app.js.map
